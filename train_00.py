from copy import deepcopy
import os
from cottonwood.structure import load_structure, save_structure, Structure
from cottonwood.loss import AbsoluteLoss
from cottonwood.normalization import MinMaxNormalization
from cottonwood.operations import Flatten
from cottonwood.experimental.ziptie_block import Ziptie
from imagenette_patches import TrainingData


reports_dir = os.path.join("reports", "train_00")
model_filename = os.path.join(reports_dir, "imagenette_ziptie.pkl")

n_iter_train = int(1e9)
n_iter_save = int(1e4)
n_bundles = 1048
threshold = 1e4


def main():
    try:
        model = retrieve_model()
    except Exception:
        model = create_model()

    train_model(model)


def retrieve_model():
    model = load_structure(model_filename)
    model.add(TrainingData(), "training_data")
    model.connect("training_data", "flatten", i_port_tail=0)
    print(f"Model restored from {model_filename}")
    return model


def create_model():
    model = Structure()

    # Handle the incoming image data
    model.add(TrainingData(), "training_data")
    model.add(Flatten(), "flatten")
    model.add(MinMaxNormalization(norm_min=0, norm_max=1), "min_max_norm")
    model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_0")
    model.add(Ziptie(n_outputs=n_bundles, threshold=threshold * 2), "ziptie_1")
    model.add(Ziptie(n_outputs=n_bundles, threshold=threshold * 4), "ziptie_2")
    model.add(Ziptie(n_outputs=n_bundles, threshold=threshold * 8), "ziptie_3")
    # model.add(Ziptie(n_outputs=n_bundles, threshold=threshold * 16), "ziptie_4")
    model.add(AbsoluteLoss(), "loss")

    model.connect_sequence([
        "training_data",
        "flatten",
        "min_max_norm",
        "ziptie_0",
        "ziptie_1",
        "ziptie_2",
        "ziptie_3",
        # "ziptie_4",
        "loss",
    ])

    return model


def train_model(model):
    os.makedirs(reports_dir, exist_ok=True)
    for i_iter in range(n_iter_train):
        model.forward_pass()
        if (i_iter + 1) % n_iter_save == 0:
            print(f"Saving model at {i_iter + 1} iterations")
            print(f'{model.blocks["ziptie_0"].algo.n_bundles} level 0 bundles')
            print(f'{model.blocks["ziptie_1"].algo.n_bundles} level 1 bundles')
            print(f'{model.blocks["ziptie_2"].algo.n_bundles} level 2 bundles')
            print(f'{model.blocks["ziptie_3"].algo.n_bundles} level 3 bundles')
            # print(f'{model.blocks["ziptie_4"].algo.n_bundles} level 4 bundles')

            model_to_save = deepcopy(model)
            model_to_save.remove("training_data")
            save_structure(model_filename, model_to_save)


if __name__ == "__main__":
    main()
