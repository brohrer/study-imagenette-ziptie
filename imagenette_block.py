import os
from PIL import Image
import numpy as np


train_dir = os.path.join("imagenette2", "train")
test_dir = os.path.join("imagenette2", "val")
label_dirnames = {
    "pump": "n03425413",
    "horn": "n03394916",
    "tench": "n01440764",
    "chainsaw": "n03000684",
    "truck": "n03417042",
    "parachute": "n03888257",
    "ball": "n03445777",
    "cassette": "n02979186",
    "spaniel": "n02102040",
    "church": "n03028079",
}
training_data = []
testing_data = []

for label, dirname in label_dirnames.items():
    train_label_dir = os.path.join(train_dir, dirname)
    test_label_dir = os.path.join(test_dir, dirname)

    filenames = os.listdir(train_label_dir)
    for filename in filenames:
        filepath = os.path.join(train_label_dir, filename)
        training_data.append((filepath, label))

    filenames = os.listdir(test_label_dir)
    for filename in filenames:
        filepath = os.path.join(test_label_dir, filename)
        testing_data.append((filepath, label))

np.random.shuffle(training_data)
np.random.shuffle(testing_data)


class TrainingData(object):
    def __init__(self):
        self.image = None
        self.label = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "Imagenette training data"

    def forward_pass(self, forward_in):
        filepath, self.label = training_data[
            np.random.choice(len(training_data))]
        self.image = np.asarray(Image.open(filepath))
        if len(self.image.shape) == 2:
            self.image = np.tile(img[:, :, np.newaxis], (1, 1, 3))
        # self.image = img.transpose(2, 0, 1)
        self.forward_out = (self.image, self.label)
        return self.forward_out

    def backward_pass(self, backward_in):
        return self.backward_out


class TestingData(object):
    def __init__(self):
        self.image = None
        self.label = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "Imagenette testing data"

    def forward_pass(self, forward_in):
        filepath, self.label = testing_data[
            np.random.choice(len(testing_data))]
        self.image = np.asarray(Image.open(filepath))
        if len(self.image.shape) == 2:
            self.image = np.tile(img[:, :, np.newaxis], (1, 1, 3))
        # self.image = img.transpose(2, 0, 1)
        self.foward_out = (self.image, self.label)
        return self.forward_out

    def backward_pass(self, arg):
        return arg


if __name__ == "__main__":

    training_data_block = TrainingData()
    for _ in range(10):
        data = training_data_block.forward_pass(None)
        img = data[0]
        label = data[1]
        print(img)
        print(label)

    testing_data_block = TestingData()
    for _ in range(10):
        img, label = testing_data_block.forward_pass(None)
        print(img)
        print(label)
