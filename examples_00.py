import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.operations import Constant
from cottonwood.structure import load_structure
import cottonwood.toolbox as tb
from imagenette_patches import TrainingData
from cottonwood.experimental.visualize_structure import render


dpi = 300
reports_dir = os.path.join("reports", sys.argv[1])
filename_base = "example"

examples_0_dir = os.path.join(reports_dir, "examples_0")
examples_1_dir = os.path.join(reports_dir, "examples_1")
examples_2_dir = os.path.join(reports_dir, "examples_2")
examples_3_dir = os.path.join(reports_dir, "examples_3")
# examples_4_dir = os.path.join(reports_dir, "examples_4")

os.makedirs(examples_0_dir, exist_ok=True)
os.makedirs(examples_1_dir, exist_ok=True)
os.makedirs(examples_2_dir, exist_ok=True)
os.makedirs(examples_3_dir, exist_ok=True)
# os.makedirs(examples_4_dir, exist_ok=True)

fig_width = 8
fig_height = 4

model_filename = os.path.join(reports_dir, "imagenette_ziptie.pkl")
model = load_structure(model_filename)
model.remove("loss")
model.add(TrainingData(), "training_data")
model.add(Constant(None), "reflect")
model.connect("training_data", "flatten", i_port_tail=0)

n_examples = 48


def show_image_pair(image_a, image_b, filename):
    fig = plt.figure(figsize=(fig_width, fig_height))
    ax_a = fig.add_axes((.05, .1, .4, .8))
    ax_b = fig.add_axes((.55, .1, .4, .8))
    ax_a.imshow(image_a, cmap="gray")
    ax_b.imshow(image_b, cmap="gray")
    plt.savefig(filename, dpi=dpi)
    plt.close()


def generate_example(block, examples_dir):
    model.connect(block, "reflect")
    for i_example in range(n_examples):
        model.forward_pass()
        model.blocks["reflect"].backward_out = model.blocks["reflect"].forward_in
        model.backward_pass()
        original_image = np.minimum(1, np.maximum(0,
            model.blocks["training_data"].forward_out[0] + .5))
        reflected_image = np.minimum(1, np.maximum(0,
            model.blocks["flatten"].backward_out))

        filename = os.path.join(
            examples_dir, f"{filename_base}_{1000 + i_example}.png")

        show_image_pair(original_image, reflected_image, filename)

    model.remove(block)

generate_example("ziptie_3", examples_3_dir)
generate_example("ziptie_2", examples_2_dir)
generate_example("ziptie_1", examples_1_dir)
generate_example("ziptie_0", examples_0_dir)
