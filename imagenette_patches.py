import os
from PIL import Image
import numpy as np


patch_size = 12
patches_per_image = 7

train_dir = os.path.join("imagenette2", "train")
test_dir = os.path.join("imagenette2", "val")
label_dirnames = {
    "pump": "n03425413",
    "horn": "n03394916",
    "tench": "n01440764",
    "chainsaw": "n03000684",
    "truck": "n03417042",
    "parachute": "n03888257",
    "ball": "n03445777",
    "cassette": "n02979186",
    "spaniel": "n02102040",
    "church": "n03028079",
}
training_data = []
testing_data = []

for label, dirname in label_dirnames.items():
    train_label_dir = os.path.join(train_dir, dirname)
    test_label_dir = os.path.join(test_dir, dirname)

    filenames = os.listdir(train_label_dir)
    for filename in filenames:
        filepath = os.path.join(train_label_dir, filename)
        training_data.append((filepath, label))

    filenames = os.listdir(test_label_dir)
    for filename in filenames:
        filepath = os.path.join(test_label_dir, filename)
        testing_data.append((filepath, label))

np.random.shuffle(training_data)
np.random.shuffle(testing_data)


def get_new_image(data):
    while True:
        filepath, label = data[
            np.random.choice(len(data))]
        image = np.asarray(Image.open(filepath))
        if len(image.shape) == 2:
            image = np.tile(image[:, :, np.newaxis], (1, 1, 3))

        # Make sure the image is big enough to support the patch
        n_image_rows, n_image_cols, _ = image.shape
        if n_image_rows > patch_size and n_image_cols > patch_size:
            return (image, label)


def get_patch(image):
    n_rows, n_cols, _ = image.shape
    i_row = np.random.randint(n_rows - patch_size)
    i_col = np.random.randint(n_cols - patch_size)
    patch = image[
        i_row: i_row + patch_size,
        i_col: i_col + patch_size, :]
    return (patch / 255) - .5


class TrainingData:
    def __init__(self):
        self.image, self.label = get_new_image(training_data)

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "Imagenette training data"

    def forward_pass(self, forward_in):
        if np.random.sample() < 1 / patches_per_image:
            self.image, self.label = get_new_image(training_data)
        self.patch = get_patch(self.image)
        self.forward_out = (self.patch, self.label)
        return self.forward_out

    def backward_pass(self, backward_in):
        return self.backward_out


class TestingData(object):
    def __init__(self):
        self.image, self.label = get_new_image(testing_data)

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "Imagenette testing data"

    def forward_pass(self, forward_in):
        if np.random.sample() < 1 / patches_per_image:
            self.image, self.label = get_new_image(testing_data)
        self.patch = get_patch(self.image)
        self.forward_out = (self.patch, self.label)
        return self.forward_out

    def backward_pass(self, arg):
        return arg


if __name__ == "__main__":

    training_data_block = TrainingData()
    for _ in range(10):
        data = training_data_block.forward_pass(None)
        img = data[0]
        label = data[1]
        print(img)
        print(label)

    testing_data_block = TestingData()
    for _ in range(10):
        img, label = testing_data_block.forward_pass(None)
        print(img)
        print(label)
