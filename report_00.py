import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.operations import Constant
from cottonwood.structure import load_structure
import cottonwood.toolbox as tb
from imagenette_patches import TrainingData
from cottonwood.experimental.visualize_structure import render


dpi = 300
reports_dir = os.path.join("reports", sys.argv[1])
filename_base = "feature"

features_0_dir = os.path.join(reports_dir, "features_0")
features_1_dir = os.path.join(reports_dir, "features_1")
features_2_dir = os.path.join(reports_dir, "features_2")
features_3_dir = os.path.join(reports_dir, "features_3")
# features_4_dir = os.path.join(reports_dir, "features_4")

os.makedirs(features_0_dir, exist_ok=True)
os.makedirs(features_1_dir, exist_ok=True)
os.makedirs(features_2_dir, exist_ok=True)
os.makedirs(features_3_dir, exist_ok=True)
# os.makedirs(features_4_dir, exist_ok=True)

fig_width = 8
fig_height = 4.5

model_filename = os.path.join(reports_dir, "imagenette_ziptie.pkl")
model = load_structure(model_filename)
model.add(TrainingData(), "training_data")
model.connect("training_data", "flatten", i_port_tail=0)

# render(model)
tb.summarize(model)

n_features = model.blocks["loss"].forward_in.size
model.remove("loss")


def show_feature(feature, filename):
    fig = plt.figure(figsize=(fig_width, fig_height))
    ax = fig.gca()
    ax.imshow(feature, cmap="gray")
    plt.savefig(filename, dpi=dpi)
    plt.close()

def show_features(block, features_dir):
    for i_feature in range(n_features):
        features = np.zeros(n_features)
        features[i_feature] = 1
        model.add(Constant(features), "feature")
        model.connect(block, "feature")

        model.backward_pass()
        expanded_feature  = np.minimum(1, np.maximum(0,
            model.blocks["flatten"].backward_out))
        model.remove("feature")

        filename = os.path.join(
            features_dir, f"{filename_base}_{1000 + i_feature}.png")

        show_feature(expanded_feature, filename)
    model.remove(block)

# show_features("ziptie_3", features_3_dir)
show_features("ziptie_2", features_2_dir)
show_features("ziptie_1", features_1_dir)
show_features("ziptie_0", features_0_dir)
